var maxX = window.innerWidth;
var maxY = window.innerHeight;

var minxWidth = 1;
var maxWidth = 301;

var speedsX = [];
var speedsY = [];

var posX;
var posY;
var offset;

function CreateBubble() {
    var bub = document.createElement('div');
    bub.style.position = 'absolute';
    var width = Math.floor(Math.random() * maxWidth) + minxWidth;
    bub.style.width = width + 'px';
    bub.style.height = bub.style.width;
    bub.style.top = Math.random() * (maxY - width) + "px";
    bub.style.left = Math.random() * (maxX - width) + "px";
    bub.style.backgroundColor = GetRandomColor();
    bub.style.borderWidth = '1px';
    bub.style.borderStyle = 'solid';
    bub.style.borderColor = GetRandomColor();
    bub.style.borderRadius = "50%";
    document.body.appendChild(bub);
}

function GetRandomColor() {
    var color = '#';
    var colors = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
    for (var i = 0; i < 8; i++) {
        color += colors[Math.floor(Math.random() * colors.length)];
    }
    return color;
}

for (var i = 0; i < 20; i++) {
    CreateBubble();
}

var bubbles = document.querySelectorAll('div');

for (var i = 0; i < bubbles.length; i++) {
    speedsX[i] = Math.random() * 6 - 3;
    speedsY[i] = Math.random() * 6 - 3;
}

function MoveBubble() {
    for (var i = 0; i < bubbles.length; i++) {

        offset = bubbles[i].style.width;
        offset = offset.substring(0, offset.length - 2);

        posX = Number(bubbles[i].style.left.substring(0, bubbles[i].style.left.length - 2));
        posY = Number(bubbles[i].style.top.substring(0, bubbles[i].style.top.length - 2));

        posX += speedsX[i];
        posY += speedsY[i];

        if (posX > maxX - offset-1 || posX < 0) {
            speedsX[i] *= -1;
        }

        if (posY > maxY - offset-1 || posY < 0) {
            speedsY[i] *= -1;
        }

        bubbles[i].style.left = posX + 'px';
        bubbles[i].style.top = posY + 'px';
    }
}

setInterval(MoveBubble, 20)
